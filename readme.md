#  NDD-NddExport

#### 介绍

插件功能：导出已着色代码为其他格式的文件

插件用途：将彩色代码，导出为word文档（RFT）或网页（HTML）文件，或者将彩色代码（RTF格式或HTMl格式）拷贝到剪贴板，粘贴到别的（word文档，HTML网页）中去。



![1](./pic/1.png)

####  QMake 编译

将该工程放在 `%ndd_root_path%/src/plugin` 文件夹中，且已经将 `QScint` 已经编译成动态库。使用 `QtCreator` 打开 `%ndd_root_path%/src/plugin/NddExport.pro` 文件即可一键编译。

**依赖项：**

- Qt Library， msvc2015_64

- QScint

  

#### vs2022 编译

点击NddExportPlugin.sln文件，启动V2022, 点击重新生成 即可一键编译。

**依赖项：**

- Qt Library， msvc2015_64

- QScint

  

#### 安装教程

将该项目生成的动态库拷贝至 `%Ndd_INSTALL_PATH%/plugin`，重启 Ndd即可。