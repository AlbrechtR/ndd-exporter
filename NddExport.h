﻿#pragma once

#include <QObject>
#include <QAction>

#define SCINTILLA_FONTNAME_SIZE	(32+1)
#define NRSTYLES	(255+1)

class QsciScintilla;
struct StyleData {
	char fontString[32];
	int fontIndex;
	int size;
	int bold;
	int italic;
	int underlined;
	int fgColor;
	int bgColor;
	int fgClrIndex;
	int bgClrIndex;
	bool eolExtend;
};

struct CurrentScintillaData {
	long nrChars;
	int tabSize;
	bool usedStyles[NRSTYLES];
	StyleData* styles;
	char* dataBuffer;
	int nrUsedStyles;
	int nrStyleSwitches;
	int totalFontStringLength;
	int currentCodePage;
	int twipsPerSpace;
};
//size definitions for memory allocation(HTML)
constexpr auto EXPORT_SIZE_HTML_STATIC = (112 + 143 + 91 + 109 + 24);	//doctype + begin header + default style + second header + footer
constexpr auto EXPORT_SIZE_HTML_UTF8 = (5);								//UTF8 meta tag
constexpr auto EXPORT_SIZE_HTML_1252 = (12);							//windows-1252 meta tag
constexpr auto EXPORT_SIZE_HTML_STYLE = (75 + 19);						//bold color bgcolor + font
constexpr auto EXPORT_SIZE_HTML_SWITCH = (27);							//<span ...></span>
constexpr auto EXPORT_SIZE_HTML_CLIPBOARD = (107 + 22 + 20);			//CF_HTML data

//size definitions for memory allocation(RTF)
constexpr auto EXPORT_SIZE_RTF_STATIC = (33 + 11 + 5 + 12 + 5 + 22 + 3);	//header + fonttbl + fonttbl end + colortbl + colortbl end + default style + eof
constexpr auto EXPORT_SIZE_RTF_STYLE = (11 + 27 + 27);						//font decl + color decl fg + color decl bg
constexpr auto EXPORT_SIZE_RTF_SWITCH = (39);								// '\f127\fs56\highlight254\cf255\b0\i0\ul0 '

union utf16struct {
	short value;
	struct {
		char byte1;
		char byte2;
	} bytes;
};
typedef utf16struct utf16;

class ExporterClass : public QObject
{
	Q_OBJECT
public:
	ExporterClass(QObject* parent, QWidget* pNotepad);
	~ExporterClass();

public:
	void doExportPDF();
	void doExportRTF();
	void doExportHTML();
	void copyExportRTFclipboard();
	void copyExportHTMLclipboard();

private:
	void fillScintillaData(QsciScintilla* scintillaEditor_);

private:
	QWidget* mainWidget_;
	CurrentScintillaData csd;
	QWidget* m_pNotepad;


};
