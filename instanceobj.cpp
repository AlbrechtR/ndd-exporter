#include "instanceobj.h"
#include <qsciscintilla.h>
#include <Scintilla.h>
#include <QWidget>
#include <QMenu>
#include "NddExport.h"

extern std::function<QsciScintilla* (QWidget*)> s_getCurEdit;

InstanceObj::InstanceObj(QWidget* pNotepad, QMenu* pMenu) :QObject(pNotepad), m_exportObj(nullptr)
{
	m_pNotepad = pNotepad;
	m_rootMenu = pMenu;
}

InstanceObj::~InstanceObj()
{
	if (m_exportObj != nullptr)
	{
		delete m_exportObj;
		m_exportObj = nullptr;
	}
}

void InstanceObj::init()
{
	if (m_exportObj == nullptr)
	{
		m_exportObj = new ExporterClass(this, m_pNotepad);
	}
}

void InstanceObj::doExportPDF()
{
	init();
	m_exportObj->doExportPDF();
}

void InstanceObj::doExportRTF()
{
	init();
	m_exportObj->doExportRTF();
}

void InstanceObj::doExportHTML()
{
	init();
	m_exportObj->doExportHTML();
}


void InstanceObj::copyExportRTFclipboard()
{
	init();
	m_exportObj->copyExportRTFclipboard();
}


void InstanceObj::copyExportHTMLclipboard()
{
	init();
	m_exportObj->copyExportHTMLclipboard();
}




