#pragma once
#include <QObject>
#include <QWidget>

class QMenu;
class ExporterClass;

class InstanceObj :public QObject
{
public:
	//外面Ndd释放时，会自动释放该对象。
	InstanceObj(QWidget* pNotepad, QMenu* pMenu);
	~InstanceObj();

	void doExportPDF();
	void doExportRTF();
	void doExportHTML();
	void copyExportRTFclipboard();
	void copyExportHTMLclipboard();
private:
	void init();

public:
	QWidget* m_pNotepad;
	QMenu* m_rootMenu;

	ExporterClass* m_exportObj;

private:
	InstanceObj(const InstanceObj& other) = delete;
	InstanceObj& operator=(const InstanceObj& other) = delete;
};